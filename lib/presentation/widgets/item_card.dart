import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rizal_test/domain/entities/pokemon_entity.dart';
import 'package:rizal_test/presentation/views/pokemon_data_view.dart';

Widget pokemonCard(
    {required PokemonEntity pokemonEntity,
    required String name,
    required Color primaryType,
    required List<dynamic> types,
    required int pokemonId,
    required String hires,
    required BuildContext context}) {
  return Card(
    clipBehavior: Clip.antiAliasWithSaveLayer,
    elevation: 0,
    margin:
        const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0, top: 10.0),
    child: Material(
      shape: RoundedRectangleBorder(
        side: const BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(10),
      ),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      color: primaryType,
      child: InkWell(
        onTap: () {
          log("Data ${pokemonEntity.toJson().toString()}");
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return PokemonDataView(pokemon: pokemonEntity);
              },
            ),
          );
        },
        child: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  color: Colors.white.withOpacity(0.15),
                  child: Container(
                    margin: const EdgeInsets.only(
                      top: 15.0,
                      bottom: 15.0,
                      right: 15.0,
                      left: 15.0,
                    ),
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(hires),
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    left: 15.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(
                          top: 0.0,
                          bottom: 5.0,
                        ),
                        child: Text(
                          name,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        width: 160,
                        height: 22.5,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: types.length,
                          itemBuilder: (context, index) {
                            final type = types[index];
                            return Card(
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              shape: RoundedRectangleBorder(
                                side: const BorderSide(
                                    color: Colors.transparent, width: 0),
                                borderRadius: BorderRadius.circular(50),
                              ),
                              elevation: 2,
                              margin: const EdgeInsets.only(
                                right: 10.0,
                              ),
                              color: primaryType,
                              child: Container(
                                color: Colors.black.withOpacity(0.1),
                                padding: const EdgeInsets.symmetric(
                                  vertical: 5.0,
                                  horizontal: 10.0,
                                ),
                                child: Text(
                                  types[index].toString().toUpperCase(),
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900,
                                    letterSpacing: 0.5,
                                    fontSize: 11.0,
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Positioned(
              right: 15,
              bottom: 25,
              child: Text(
                "#" + pokemonId.toString().padLeft(3, '0'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 32,
                  color: Colors.white.withOpacity(0.5),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
