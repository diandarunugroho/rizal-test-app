import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:rizal_test/domain/entities/pokemon_entity.dart';
import 'package:rizal_test/presentation/controllers/favorited_pokemon_list_view_controller.dart';
import 'package:rizal_test/presentation/controllers/pokemon_list_view_controller.dart';
import 'package:rizal_test/presentation/views/pokemon_data_view.dart';
import 'package:rizal_test/presentation/widgets/item_card.dart';

class PokemonListView extends StatefulWidget {
  const PokemonListView({Key? key}) : super(key: key);

  @override
  State<PokemonListView> createState() => _PokemonListViewState();
}

class _PokemonListViewState extends State<PokemonListView> {
  List<PokemonEntity> favoritedsPokemon = [];

  @override
  void initState() {
    super.initState();
    favoritedsPokemon =
        FavoritedPokemonListViewController.favoritedPokemonList.value;
    if (PokemonListViewController.isLoadingList == false)
      PokemonListViewController.getPokemonList(100);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Pokemon List'),
        ),
        body: ValueListenableBuilder<List<PokemonEntity>>(
            valueListenable: PokemonListViewController.pokemonList,
            builder: (context, pokemonList, _) {
              return ListView.builder(
                itemCount: pokemonList.length,
                itemBuilder: (BuildContext context, int index) {
                  var types = [pokemonList[index].type1.name];
                  if (pokemonList[index].type2?.name != null) {
                    types.add(pokemonList[index].type2!.name);
                  }

                  return GestureDetector(
                    onTap: () {
                      log("Data ${pokemonList[index].toJson().toString()}");
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return PokemonDataView(pokemon: pokemonList[index]);
                          },
                        ),
                      );
                    },
                    child: pokemonCard(
                        pokemonEntity: pokemonList[index],
                        name: pokemonList[index].name,
                        primaryType: pokemonList[index].type1.primaryColor,
                        types: types,
                        pokemonId: pokemonList[index].id,
                        hires: pokemonList[index].oficialArtWorkUrl,
                        context: context),
                  );
                },
              );
            }),
      ),
    );
  }

// Widget pokemonCard({
//   required String name,
//   required String primaryType,
//   required List<dynamic> types,
//   required int pokemonId,
//   required String hires,
// }) {
//   return Card(
//     clipBehavior: Clip.antiAliasWithSaveLayer,
//     elevation: 0,
//     margin: const EdgeInsets.only(
//       left: 20.0,
//       right: 20.0,
//       bottom: 15.0,
//     ),
//     child: Material(
//       shape: RoundedRectangleBorder(
//         side: const BorderSide(color: Colors.transparent, width: 0),
//         borderRadius: BorderRadius.circular(10),
//       ),
//       clipBehavior: Clip.antiAliasWithSaveLayer,
//       color: getPrimaryTypeColor(primaryType),
//       child: InkWell(
//         onTap: () {
//           Navigator.push(
//             context,
//             MaterialPageRoute(
//               builder: (context) => PokemonDetailPage(pokemonId - 1, pokemonList, typesList),
//             ),
//           );
//         },
//         child: Stack(
//           children: [
//             Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: [
//                 Container(
//                   color: Colors.white.withOpacity(0.15),
//                   child: Container(
//                     margin: const EdgeInsets.only(
//                       top: 15.0,
//                       bottom: 15.0,
//                       right: 15.0,
//                       left: 15.0,
//                     ),
//                     width: 60,
//                     height: 60,
//                     decoration: BoxDecoration(
//                       image: DecorationImage(
//                         image: CachedNetworkImageProvider(hires),
//                         alignment: Alignment.center,
//                         fit: BoxFit.contain,
//                       ),
//                     ),
//                   ),
//                 ),
//                 Container(
//                   margin: const EdgeInsets.only(
//                     left: 15.0,
//                   ),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Container(
//                         margin: const EdgeInsets.only(
//                           top: 0.0,
//                           bottom: 5.0,
//                         ),
//                         child: Text(
//                           name,
//                           style: TextStyle(
//                             fontSize: 22,
//                             color: Colors.white,
//                             fontWeight: FontWeight.bold,
//                           ),
//                         ),
//                       ),
//                       Container(
//                         width: 160,
//                         height: 22.5,
//                         child: ListView.builder(
//                           scrollDirection: Axis.horizontal,
//                           itemCount: types.length,
//                           itemBuilder: (context, index) {
//                             final type = types[index];
//                             return Card(
//                               clipBehavior: Clip.antiAliasWithSaveLayer,
//                               shape: RoundedRectangleBorder(
//                                 side: const BorderSide(color: Colors.transparent, width: 0),
//                                 borderRadius: BorderRadius.circular(50),
//                               ),
//                               elevation: 2,
//                               margin: const EdgeInsets.only(
//                                 right: 10.0,
//                               ),
//                               color: getPrimaryTypeColor(types[index]),
//                               child: Container(
//                                 color: Colors.black.withOpacity(0.1),
//                                 padding: const EdgeInsets.symmetric(
//                                   vertical: 5.0,
//                                   horizontal: 10.0,
//                                 ),
//                                 child: Text(
//                                   types[index].toString().toUpperCase(),
//                                   style: TextStyle(
//                                     color: Colors.white,
//                                     fontWeight: FontWeight.w900,
//                                     letterSpacing: 0.5,
//                                     fontSize: 11.0,
//                                   ),
//                                 ),
//                               ),
//                             );
//                           },
//                         ),
//                       ),
//                     ],
//                   ),
//                 )
//               ],
//             ),
//             Positioned(
//               right: 15,
//               bottom: 25,
//               child: Text(
//                 "#" + pokemonId.toString().padLeft(3, '0'),
//                 style: TextStyle(
//                   fontWeight: FontWeight.bold,
//                   fontSize: 32,
//                   color: Colors.white.withOpacity(0.5),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ),
//   );
}
