import 'package:flutter/material.dart';
import 'package:rizal_test/core/ui/scale.dart';
import 'package:rizal_test/presentation/views/pokemon_list_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rizal Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Builder(builder: (BuildContext context) {
        Scale.init(context);
        return const PokemonListView();
      }),
    );
  }
}
